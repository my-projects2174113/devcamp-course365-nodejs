# Project Course 365

## 🌟 Code Explanation

> ### 1. Front-End

- **_Trang chủ(Home Page)_**
- Khi tải trang thì call API lấy dữ liệu toàn bộ khóa học rồi lưu vào biến toàn cục.
- Sau đó gọi 3 hàm riêng biệt với tham số là biến toàn cục đó để load dữ liệu các khóa học vào 3 hạng mục: "Recommend", "Popular", "Trending".
- Với các khóa "Recommend" thì sử dụng vòng lặp forEach để lặp qua các phần tử của mảng, kết hợp biến đếm để đảm bảo chỉ hiển thị tối đa 4 khóa học.
- Với các khóa "Popular" thì sử dụng vòng lặp forEach để lặp qua các phần tử của mảng tìm kiếm phần tử có thuộc tính isPopular = true, kết hợp biến đếm để đảm bảo chỉ hiển thị tối đa 4 khóa học.
- Với các khóa "Trending" thì sử dụng vòng lặp forEach để lặp qua các phần tử của mảng tìm kiếm phần tử có thuộc tính isTrending = true, kết hợp biến đếm để đảm bảo chỉ hiển thị tối đa 4 khóa học.
- Với mỗi phần tử phù hợp sẽ tạo 1 thẻ card chứa thông tin khóa học. Tạo 1 hàm riêng cho việc tạo thẻ card này để rút gọn code và đồng nhất cấu trúc thẻ card.
- Trong thẻ card có sử dụng tooltip và overflow: ellipse để đảm bảo nội dung ko bị tràn khỏi thẻ cha khi kích cỡ màn bị thu nhỏ.

- **_Trang admin(Admin Page)_**
- _Hiển thị danh sách khóa học_
> - Khởi tạo DataTable.
> - Render thông tin ở các cột để hiển thị như mong muốn.
> - Call API lấy danh sách khóa học rồi lưu vào biến toàn cục, load dữ liệu vào bảng. Trong lúc chờ response sẽ hiển thị spinner để người dùng biết. Nếu lỗi sẽ thông báo.
> - Mỗi lần tải trang sẽ load lại dữ liệu bảng 1 lẩn.

- _Tạo mới khóa học_
> - Lắng nghe sự kiện click nút thêm mới.
> - Hiển thị modal thêm mới khóa học.
> - Lắng nghe sự kiện input ở 2 ô coverImage và teacherPhoto, từ đó cập nhật luôn hình ảnh ở bên dưới để giao diện trực quan hơn cho người dùng. Sử dụng hàm debounce để giảm độ lag khi sự kiện input xảy ra quá nhanh do người dùng gõ nhanh.
> - Lắng nghe sự kiện click nút create trên form thêm mới.
> - Khi bấm nút thì sẽ thực hiện 3 bước xử lý sự kiện là thu thập dữ liệu, validate dữ liệu trên form và call API.
> - Khi call API sẽ hiển thị spinner để người dùng biết mà chờ. Khi thành công sẽ thông báo rồi ẩn modal và spinner, sau đó sẽ gọi hàm load lại dữ liệu trong bảng.
> - Lắng nghe sự kiện ẩn modal create ở global, cứ ẩn là xóa trắng thông tin trên modal.

> - _Edit khóa học_
> - Lắng nghe sự kiện click nút edit.
> - Hiển thị modal edit khóa học.
> - Sử dụng DataTable để lấy Id khóa học tương ứng rồi call API để lấy thông tin khóa học.
> - Lấy được thông tin khóa học rồi thì sẽ tiến hành đổ vào form trên modal edit.
> - Lắng nghe sự kiện input ở 2 ô coverImage và teacherPhoto, từ đó cập nhật luôn hình ảnh ở bên dưới để giao diện trực quan hơn cho người dùng. Sử dụng hàm debounce để giảm độ lag khi sự kiện input xảy ra quá nhanh do người dùng gõ nhanh.
> - Lắng nghe sự kiện click nút update trên form thêm mới.
> - Khi bấm nút thì sẽ thực hiện 3 bước xử lý sự kiện là thu thập dữ liệu, validate dữ liệu trên form và call API.
> - Khi call API sẽ hiển thị spinner để người dùng biết mà chờ. Khi thành công sẽ thông báo rồi ẩn modal và spinner, sau đó sẽ gọi hàm load lại dữ liệu trong bảng. Nếu lỗi thì sẽ thông báo.
> - Lắng nghe sự kiện ẩn modal edit ở global, cứ ẩn là xóa trắng thông tin trên modal.

- _Xóa khóa học_
> - Lắng nghe sự kiện click nút xóa.
> - Hiển thị modal xóa khóa học.
> - Sử dụng DataTable để lấy Id khóa học tương ứng.
> - Lắng nghe sự kiện click nút delete trên form delete.
> - Khi bấm xóa thì sẽ thực hiện call API để xóa.
> - Khi call API sẽ hiển thị spinner để người dùng biết mà chờ. Khi thành công sẽ thông báo rồi ẩn modal và spinner, sau đó sẽ gọi hàm load lại dữ liệu trong bảng. Nếu lỗi thì sẽ thông báo.

- _Lọc khóa học_
> - Lắng nghe sự kiện click 2 nút filter và reset.
> - Khi click nút filter sẽ thực hiện thu thập value của 2 ô select(popular & trending), sau đó gọi hàm lọc dữ liệu.
> - Hàm lọc sử dụng method filter để lọc mảng danh sách khóa học ở global đã lưu ban đầu. Từ đó gán vào 1 mảng mới những giá trị phù hợp. Gọi hàm load dữ liệu vào table với mảng mới đó.
> - Khi click nút reset sẽ gọi lại hàm load toàn bộ khóa học vào bảng và đặt lại value của 2 thẻ select thành "false".

> ### 2. Back-End
- **_Models_**
- Import thư viện Mongoose
- Khai báo đối tượng Schema
- Khởi tạo một Schema trong Mongoose với các thuộc tính cần thiết.
- Tạo 1 Model đối với Schema vừa khởi tạo và exports.
> ### 2. Controllers
- Import thư viện Mongoose và các Model cần thiết.
- Định nghĩa các hàm bất đồng bộ để xử lý các yêu cầu call API.
- Các queries trong Mongoose là bất đồng bộ nên sẽ cần sử dụng await trước mỗi truy vấn query.
- Đặt toàn bộ hàm trong khối try-catch để bắt lỗi hiệu quả tránh treo server khi xảy ra lỗi bất ngờ.
- Nếu đối tượng có các trường tham chiếu đến Model khác, sử dụng populate() để lấy thông tin đầy đủ(có thể populate() lồng nhau hoặc nhiều Model 1 lúc).
- Mỗi khi có lỗi xảy ra thì sẽ xử lý phản hồi tới client và code sẽ ngừng ngay lập tức.
- Một số status chính dùng để phản hồi tới client là:
  > - 200: Khi response trả về thành công
  > - 201: (Chỉ dành cho POST) Khi tạo dữ liệu thành công
  > - 204: (Chỉ dành cho DELETE) Khi xóa thành công. Ta cũng có thể sử dụng status 200 cho DELETE nếu như muốn phản hồi thông điệp về cho client
  > - 400: Khi input truyền vào từ request (query, params hoặc body JSON) không hợp lệ
  > - 404: Khi không tìm thấy bản ghi trên CSDL
  > - 500: Khi code bị lỗi
- Trong mỗi hàm sẽ có thứ tự xử lý như sau:
  > - _Bước 1: Thu thập dữ liệu_
  > - _Bước 2: Kiểm tra dữ liệu_
  > - _Bước 3: Xử lý dữ liệu_
- Sẽ có các controller cơ bản như sau:
  > - _Get Alls_
  > - _Get By Id_
  > - _Create_
  > - _Update_
  > - _Delete_
  >
> ### 3. Middlewares
- Middleware in ra method của request.
- Middleware in ra thời gian của request.
> ### 4. Routes
- Import thư viện express
- Import controllers vừa tạo sử dụng phép gán phá hủy cấu trúc đối tượng
- Khởi tạo 1 router bằng express
- Dùng phương thức router.<method>() (trong đó <method> là phương thức HTTP như post, get, put, delete) để định nghĩa các endpoint API và xác định các handlers(controllers) cho chúng
- Gán các hàm xử lý (handlers) tương ứng cho mỗi endpoint API
- Định nghĩa đường dẫn cho mỗi endpoint sử dụng format Restful API
> ### 5. App
- File khởi tạo chính của dự án
- Import các thư viện express, mongoose, path, cors
- Import các router
- Định nghĩa các biến app và port
- Thêm middleware express.json(), express.static(), cors() và 2 custom middleware vừa định nghĩa.
- Trỏ đường dẫn tới tới file home page
- Định nghĩa các router API
- Kết nối với MongoDB
- Khởi tạo dự án chạy trong môi trường NodeJs, dữ liệu server sẽ được lắng nghe tại cổng 8000(port) để tránh nhầm lẫn với các cổng hệ thống.

## 📄 Description

> ### 1. Tổng quan
- Trang bắt đầu của dự án là trang Home Page
- Hiển thị thông tin các khóa học cho người dùng, với 3 hạng mục khóa học chính là "Recommend", "Popular", "Trending"
- Trang Admin Page sẽ có thể theo dõi danh sách khóa học, tạo mới, sửa và xóa khóa học, lọc khóa học theo độ phổ biến và độ hot(popular & trending)
> ### 2. Cách tổ chức thư mục dự án
- Sử dụng mô hình MVC (Model-View-Controller) trong NodeJs để tổ chức thư mục dự án.
- File khởi tạo chính của dự án là _index.js_.
- Thư mục app chứa 4 tổ chức file chính đó là _routes_, _middleware_, _controllers_, _models_.
- Các file trong dự án liên kết với nhau theo chuẩn CommonJS.
> ### 3. Luồng thực thi của dự án
- Dự án sẽ được bắt đầu từ file chính index.js
- Client sẽ sử dụng API để tương tác với server
- API sau khi được gọi sẽ lần lượt chạy qua routes -> middlewares -> controllers -> CSDL.

## ✨ Feature
> ### 1. Trang chủ(Home Page)
- ***Main interface***: _Giao diện chính_
  ![Home](./views/images/markdown/user/home.png)
- ***Courses List***: _Danh sách các khóa học_
  ![Course](./views/images/markdown/user/recomend.png)
  ![Course](./views/images/markdown/user/popular.png)
  ![Course](./views/images/markdown/user/trending.png)
- ***Responsive***: _Giao diện tương thích nhiều kích cỡ màn hình_
  ![Responsive](./views/images/markdown/user/responsive_md.png)
  ![Responsive](./views/images/markdown/user/responsive_sm.png)

> ### 2. Trang admin(Admin Page)
- ***Main interface***: _Giao diện chính_
  ![Home](./views/images/markdown/admin/spinner_table.png)
  ![Home](./views/images/markdown/admin/home.png)
- ***Create Course***: _Thêm mới khóa học_
  ![Create](./views/images/markdown/admin/create.png)
  ![Create](./views/images/markdown/admin/create_validate.png)
  ![Create](./views/images/markdown/admin/create_success.png)
- ***Edit Course***: _Edit khóa học_
  ![Edit](./views/images/markdown/admin/edit.png)
  ![Edit](./views/images/markdown/admin/edit_validate.png)
  ![Edit](./views/images/markdown/admin/edit_success.png)
- ***Delete Course***: _Xóa khóa học_
  ![Delete](./views/images/markdown/admin/delete.png)
  ![Delete](./views/images/markdown/admin/delete_success.png)
- ***Filter Course***: _Lọc khóa học_
  ![Filter](./views/images/markdown/admin/filter.png)
  ![Filter](./views/images/markdown/admin/reset.png)
- ***Responsive***: _Giao diện tương thích nhiều kích cỡ màn hình_
  ![Responsive](./views/images/markdown/admin/responsive.png)

## 🧱 Technology

> ### Front-end:
> 1. [Bootstrap 5](https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css)
> 2. [DataTable](https://cdn.datatables.net/1.13.8/css/dataTables.bootstrap5.min.css)
> 3. [Jquery 3](https://code.jquery.com/jquery-3.7.1.js)
> 4. [Lodash](https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.21/lodash.min.js)
> 5. [Font Awesome 6](https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css)
> 6. Ajax
> 7. JSON
> 8. Javascript

> ### Back-end:
> 1. [Node.js](https://nodejs.org/en)
> 2. [Express.js(4.18.3)](https://expressjs.com/)
> 3. [Mongoose.js(8.2.1)](https://mongoosejs.com/)
> 4. [Cors(2.8.5)](https://expressjs.com/en/resources/middleware/cors.html)
> 5. [UUID(9.0.1)](https://www.npmjs.com/package/uuid)
> 6. [Nodemon(3.1.0)](https://www.npmjs.com/package/nodemon)

> ### Database:
> 1. [NoSQL - MongoDB](https://www.mongodb.com/)
